angular.module('ordnaFIKA-firebase.controllers', [])

.controller('GlobalCtrl', function($scope, $injector) {
  var loginService = $injector.get('loginService');
  $scope.isLoggedIn = function() {
    var isLoggedIn = loginService.getAuthData();
    if(isLoggedIn.uid) {
      return true;
    }
    else {
      return false;
    }
  };
})

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('LoginCtrl', function($scope, loginService) {
  $scope.onLogin = function(provider) {
    loginService.login(provider);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope, $injector, $state) {
  var loginService = $injector.get('loginService');
  $scope.settings = {
    enableFriends: true
  };
  $scope.logmeOut = function() {
    loginService.logout();
    $state.go('tab.login');
  }
  $scope.closeDeleteAccount = function() {
    loginService.closeDeleteAccount();
    $state.go('tab.login');
  }
});
