angular.module('ordnaFIKA-firebase.services', [])

/* Provides dataref that is then passed around to other services/controllers as needed */
.factory('datarefService', function($injector) {
  var myDataRef = new Firebase('https://blinding-fire-3435.firebaseio.com/');
  var constLookUpStrings = {
    "USERS": "users",
    "FRIENDS": "friends",
    "GROUPS": "groups",
    "EVENTS": "events"
  };
  return {
    getDataRef: function() {
      return myDataRef;
    },
    getLookUpStrings: function(str2LookUp) {
      return constLookUpStrings[str2LookUp];
    }
  };
})


/* Provides utility service for CRUD operations */
.factory('databaseService', function($injector) {
  var myDataRef = $injector.get('datarefService').getDataRef();
  var usersTable = $injector.get('datarefService').getLookUpStrings("USERS");
  return {
      getUserFromDatabase: function(uid) {
        var usersRef = myDataRef.child(usersTable + "/" + uid);
        usersRef.once('value', function(user) {
          return (user.val());
        });
      },
      setUserInDatabase: function(authData) {
        var usersRef = myDataRef.child(usersTable).child(authData.uid);
//        var usersRef = myDataRef.child("users").child(authData.uid);
        usersRef.set({
          "authData": authData,
          "displayName": authData[authData.provider].displayName,
          "profileImageURL": authData[authData.provider].profileImageURL,
          "provider": authData.provider
        });
      },
      removeUserFromDatabase: function(uid) {
        var usersRef = myDataRef.child(usersTable + "/" + uid);
        console.log(usersRef.toString());
        usersRef.remove();
      }
  };
})

/* Provides utility functions related to login to firebase */
.factory('loginService', function($state, $injector, $window) {
  var myDataRef = $injector.get('datarefService').getDataRef();
  var dbService = $injector.get('databaseService');
  var uid = null;
  var loggedIn = uid?true:false;
  return {
    isLoggedIn: function() {
      return loggedIn;
    },
    getAuthData: function() {
      var result = myDataRef.getAuth();
      loggedIn = result.auth.uid?result:false;
      return loggedIn?result:false;
    },
    loginCallback: function(error, authData) {
      if(!authData || error) {
        isLoggedIn = false;
        alert("Authentication Denied. Please try again..." + error);
        return;
      }
      uid = authData.uid;
      loggedIn = true;
      dbService.setUserInDatabase(authData);
      // TODO: Improve on the code here. Redirects should not be
      // Done from the service!
      $state.go('tab.dash');
    },
    login: function(provider) {
      if(!loggedIn) {
        myDataRef.authWithOAuthPopup(provider, this.loginCallback);
      } else {
        return loggedIn;
      }
    },
    closeDeleteAccount: function() {
      if(loggedIn) {
        loggedIn = false; provider = "";
        dbService.removeUserFromDatabase(uid);
        // TODO: Improve on the code here. Redirects should not be
        // Done from the service!
        $state.go('tab.login');
        $window.location.reload(true);
      }
    },
    logout: function() {
      provider = "";
      isLoggedIn = false;
      myDataRef.unauth();
      $state.go('tab.login');
      $window.location.reload(true);
    }
  };
})

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Mike Harrington',
    lastText: 'This is wicked good ice cream.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
